<?php
class Employee{

    // database connection and table name
    private $conn;
    private $table_name = "MsEmployee";

    // object properties
    public $id;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // select ms employee
    function select_ms_employee(){
        // select all query
      $query = "SELECT
                  `id`, `fullname`, `gender`, `title`, `language`, `job_title`, `university`
              FROM
                  " . $this->table_name . "
              ORDER BY id";

      // prepare query statement
      $stmt = $this->conn->prepare($query);
      // execute query
      $stmt->execute();
      return $stmt;
    }
}
