<?php
 if($_SERVER["REQUEST_METHOD"] != "POST"){
   die();
 }

// include database and object files
include_once '../../config/database.php';
include_once '../model/auth.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

$method = isset($_GET['method']) ? $_GET['method'] : die();

try{
  if($method == "login"){
    login($db);
  }
}catch(Exception $ex){
  $result_arr=array(
      "result" => null,
      "error" => $ex->getMessage(),
  );
  header('Content-Type: application/json');
  print_r(json_encode($result_arr));
}

function login($db){
  // prepare user object
  $auth = new Auth($db);
  // set ID property of user to be edited
  $auth->username = isset($_POST['username']) ? $_POST['username'] : dieParam(); //die()
  $auth->password = isset($_POST['password']) ? $_POST['password'] : dieParam();
  // read the details of user to be edited
  $stmt = $auth->login();
  if($stmt->rowCount() > 0){
      // get retrieved row
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      // create array
      $user_arr=array(
        "id" => $row['id'],
        "username" => $row['username'],
        "fullname" => $row['fullname'],
        "address" => $row['address'],
        "email" => $row['email']
      );

      $result_arr=array(
        "result" => $user_arr,
        "error" => null
      );
  }
  else{
    throwException("Username or password is incorrect");
  }
  // make it json format
  header('Content-Type: application/json');
  print_r(json_encode($result_arr));
}

function throwException($ex){
  throw new Exception($ex);
}

function dieParam(){
  throw new Exception("Parameter is missing!");
}
?>
