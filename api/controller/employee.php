<?php
 if($_SERVER["REQUEST_METHOD"] != "POST"){
   die();
 }

// include database and object files
include_once '../../config/database.php';
include_once '../model/employee.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

$method = isset($_GET['method']) ? $_GET['method'] : die();

try{
  if($method == "select_ms_employee"){
    select_ms_employee($db);
  }
}catch(Exception $ex){
  $result_arr=array(
      "result" => null,
      "error" => $ex->getMessage(),
  );
  header('Content-Type: application/json');
  print_r(json_encode($result_arr));
}

function select_ms_employee($db){
  // prepare user object
  $employee = new Employee($db);

  // read the details of user to be edited
  $stmt = $employee->select_ms_employee();
  if($stmt->rowCount() > 0){
      // get retrieved row
      $employee_arr = array();

      while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $employee_arr[] = $row;
      }

      $result_arr=array(
        "result" => $employee_arr,
        "error" => null
      );
  }
  else{
    throwException("Empty Data");
  }
  // make it json format
  header('Content-Type: application/json');
  print_r(json_encode($result_arr));
}

function throwException($ex){
  throw new Exception($ex);
}

function dieParam(){
  throw new Exception("Parameter is missing!");
}
?>
