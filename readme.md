# PHP API First Project

Project ini adalah contoh aplikasi sederhana REST API untuk menampilkan data dalam bentuk JSON, yang nantinya akan digunakan oleh aplikasi mobile. Aplikasi ini dibuat menggunakan PHP 7 dengan XAMPP 7.1.27 Rev. 1.

## Installation

Untuk instalasi, pertama lakukan install XAMPP versi terbaru. Lalu import atau restore database MySql yang ada di folder database. File SQL-nya bernama FirstProject.sql.

Lalu gunakan aplikasi postman untuk memanggil REST API tersebut dengan POST Method.

Isi parameternya di bagian body, lalu pilih yang form-data. Kemudian isi
- KEY : "username"  VALUE : "admin"
- KEY : "password"  VALUE : "admin"

Berikut urlnya untuk dipanggil dalam POSTMAN. Masukkan IP local anda sebagai hostnya. Jangan menggunakan "localhost", karena jika menggunakan localhost, tidak bisa dipanggil dalam aplikasi mobile walaupun anda menggunakan emulator di device yang sama dengan servernya.
```
http://10.98.0.154/firstproject/api/controller/auth.php?method=login
```

Jika berhasil akan muncul response JSON seperti berikut :
```
{
    "result": {
        "id": "1",
        "username": "admin",
        "fullname": "admin admin",
        "address": "jl admin blok a/1",
        "email": "admin@adm.com"
    },
    "error": null
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[SUMEDCODING.COM](http://sumedcoding.com/)
